import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  color: Array<string> = ['#2BBBAD', '#FF8800', '#63cbd3', '#610061', '#00C851'];

  constructor()
  {

  }

}
