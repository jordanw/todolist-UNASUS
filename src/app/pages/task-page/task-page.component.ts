import { Component, OnInit } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from "angularfire2/firestore";
import {APIFireService} from "../../apifire.service";
import {Router} from "@angular/router";
import {NgZone} from "@angular/core";
import {Subject} from "rxjs/index";
import {Observable} from "rxjs/index";

@Component({
  selector: 'app-task-page',
  templateUrl: './task-page.component.html',
  styleUrls: ['./task-page.component.css']
})
export class TaskPageComponent implements OnInit {

  _title: string;
  work: string;
  taskcollection : AngularFirestoreCollection<any>;
  tasks;
  searchterm;
  startAt = new Subject();
  endAt = new Subject();
  isOpened = true;
  query: AngularFirestoreCollection<any>;
  dados:Observable<any>;
  showForm:boolean = false;

  constructor
  (
    private afs: AngularFirestore,
    private api:APIFireService,
    private r: Router,
    private zone:NgZone
  )
  {
    console.log('inside the page task');
    this.taskcollection = afs.collection('tasks');
    this.tasks = this.taskcollection.valueChanges();
  }

  toggle()
  {
    this.showForm = !this.showForm;
  }

  delete(id:string)
  {
    this.api.delete(id);
  }


  taskEdit(task)
  {
    this.r.navigate(['/edit', {
      id : task.taskId,
      title: task.title,
      work: task.work
    }]);
  }

  ngOnInit() {

  }

  search($event)
  {
    let q = $event.target.value;
    this.startAt.next(q);
    this.endAt.next(q +'\uf8ff');
    if(q != '' || q != undefined)
    {
      this.isOpened = false;
    }
    if(q == '' || q == undefined)
    {
      this.isOpened = true;
    }

    this.query = this.afs.collection('tasks', ref=>{
      return ref.where("title", "==", q);
    });
    this.dados = this.query.valueChanges();

    console.log('Valor de open: ', this.isOpened);
    console.log('Valor de q: ', q, typeof q);
    console.log('Valor dos dados: ', this.dados);
  }

}
