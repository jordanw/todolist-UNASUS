import { Component, OnInit } from '@angular/core';
import {APIFireService} from "../../apifire.service";
import {Router, ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {

  titleEdited;
  workEdited;

  constructor(
    private api:APIFireService,
    private router:ActivatedRoute,
    private router2: Router,
    private location: Location) {
    this.router.params.subscribe((v)=>{
      this.titleEdited = v.title;
      this.workEdited = v.work;
    });
  }

  updateTask()
  {
    this.router.params.subscribe((v)=>{
      console.log('VALORES DE V EM EDIT: ', v);
      this.api.update(v.id, this.titleEdited, this.workEdited);
    });
    this.location.back();
  }

  goBack()
  {
    this.location.back();
  }

  ngOnInit() {
  }

}
