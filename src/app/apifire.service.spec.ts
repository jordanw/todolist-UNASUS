import { TestBed, inject } from '@angular/core/testing';

import { APIFireService } from './apifire.service';

describe('APIFireService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [APIFireService]
    });
  });

  it('should be created', inject([APIFireService], (service: APIFireService) => {
    expect(service).toBeTruthy();
  }));
});
