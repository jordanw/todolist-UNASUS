import { NgModule } from '@angular/core';
import {RouterModule, Routes, Router} from "@angular/router";
import {EditTaskComponent} from "./pages/edit-task/edit-task.component";
import {TaskPageComponent} from "./pages/task-page/task-page.component";

const routes: Routes = [
  {path: 'edit', component: EditTaskComponent},
  {path: 'task', component: TaskPageComponent},
  //{path: '', redirectTo: '/task', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule {

  constructor(private route: Router)
{

}


}
