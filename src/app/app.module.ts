import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { AngularFireModule} from 'angularfire2';
import { AngularFirestoreModule} from 'angularfire2/firestore';
import { environment} from '../environments/environment';

import { AppRoutingModule } from './/app-routing.module';
import { APIFireService } from "./apifire.service";
import {EditTaskComponent} from "./pages/edit-task/edit-task.component";
import {TaskPageComponent} from "./pages/task-page/task-page.component";
import {Form} from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    EditTaskComponent,
    TaskPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AppRoutingModule
  ],
  providers: [APIFireService],
  bootstrap: [AppComponent]
})
export class AppModule { }
