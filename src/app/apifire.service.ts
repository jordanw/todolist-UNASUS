import {Injectable, NgModule} from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import {Router, RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  //{path: 'edit', component: EditTaskComponent},
  //{path: 'teste', component: TesteComponent},
];

@NgModule({
  //imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

@Injectable({
  providedIn: 'root'
})
export class APIFireService {

  _title: string;
  work: string;
  taskcollection : AngularFirestoreCollection<any>;
  tasks;

  constructor(afs: AngularFirestore, private router: Router)
  {
    this.taskcollection = afs.collection('tasks');
    this.tasks = this.taskcollection.valueChanges().subscribe((v)=>{
      console.log('valores no firestore: ', v);
      console.log('ids no firestore: ', v['taskId']);
    });
  }

  add(_title, work)
  {
    this.taskcollection.add({
      //title: this._title,
      //work: this.work
      title: _title,
      work: work
    })
      .then((taskRef)=>{
        this.taskcollection.doc(taskRef.id).update({
          taskId: taskRef.id
        });
      })
      .catch((err) => {
        console.log('error: ', err);
      });
  }

  update(key:string, title:any, work:any)
  {
    this.taskcollection.doc(key).update({
      title: title,
      work: work
    }).then(()=>{
      console.log('atualizado');
    }).catch(()=>{
      console.log('Erro ao atualizar dados');
    });
  }

  delete(id)
  {
    this.taskcollection.doc(id).delete().then(()=>{
      console.log('DELETED');
    }).catch(()=>{
      console.log('FALHA AO DELETAR');
    });
  }

}
